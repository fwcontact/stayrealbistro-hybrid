angular.module('starter')

    .constant("CONSTANTS",
    {
        "IMAGE_PATH": '/images'
    })

    .factory('API', [
        function () {
            return {entry: 'http://119.75.5.134:8080/stayrealbistro'};
            //return {entry: 'http://localhost:8080/stayrealbistro'};
        }
    ])

    .factory('PosTmpMasFactory',
    ['$resource',
        function ($resource) {
            var factory = {};
            factory.newResource = function (entry) {
                return $resource(entry + '/PosTmpMas');
            };
            return factory;
        }])

    .factory('PosTmpItemFactory',
    ['$resource',
        function ($resource) {
            var factory = {};
            factory.newResource = function (entry) {
                return $resource(entry + '/PosTmpItem',
                    {},
                    {
                        save: {
                            method:'POST',
                            url: entry + '/PosTmpItem/genPoshold'
                        }
                    }
                );
            };
            return factory;
        }])

    .factory('PosShopMasFactory',
    ['$resource',
        function ($resource) {
            var factory = {};
            factory.newResource = function (entry) {
                return $resource(entry + '/PosShopMas');
            };
            return factory;
        }])

    .factory('PosShopZoneFactory',
    ['$resource',
        function ($resource) {
            var factory = {};
            factory.newResource = function (entry) {
                return $resource(entry + '/PosShopZone');
            };
            return factory;
        }])

    .factory('AppData',
    ['PosTmpMasFactory', 'PosShopMasFactory', 'API',
        function (PosTmpMasFactory, PosShopMasFactory, API) {
            return {
                // home menu items
                homeMenuItems: [],

                // order items
                orderItems: [],

                // login data
                loginData: {posNo: '', orgId: '', shopId: '', apiEntry: API.entry},

                // reload home menu items
                reloadHomeMenuItems: function () {
                    // clear first
                    this.homeMenuItems.length = 0;
                    // reload
                    console.log('reload - loginData.orgId', this.loginData.orgId);
                    if (this.loginData.orgId) {
                        var refThis = this;
                        PosTmpMasFactory.newResource(this.loginData.apiEntry).query(
                            {orgId: this.loginData.orgId},
                            function (results) {
                                //On successful callback run code
                                console.log('reload success');
                                angular.forEach(results, function (result) {
                                    refThis.homeMenuItems.push(result);
                                });
                            },
                            function () {
                                //Do error handling here
                                console.log('reload failed');
                            });
                    }
                },

                // login
                login: function () {
                    // clear order first
                    this.orderItems.length = 0;
                    // login
                    console.log('login - loginData.posNo', this.loginData.posNo);
                    console.log('login - loginData.apiEntry', this.loginData.apiEntry);
                    API.entry = this.loginData.apiEntry;
                    console.log('new entry', API.entry);
                    if (this.loginData.posNo) {
                        var refLoginData = this.loginData;
                        var refThis = this;
                        PosShopMasFactory.newResource(this.loginData.apiEntry).get(
                            {posNo: this.loginData.posNo},
                            function (response) {
                                //On successful callback run code
                                refLoginData.orgId = response.orgId;
                                refLoginData.locId = response.locId;
                                refLoginData.storeId = response.storeId;
                                refLoginData.shopId = response.shopId;
                                refLoginData.shopName = response.shopName;
                                refLoginData.address1 = response.address1;
                                refLoginData.phone = response.phone;
                                refLoginData.fax = response.fax;
                                refLoginData.emailAddr = response.emailAddr;
                                console.log('login success', refLoginData);
                                // auto reload
                                refThis.reloadHomeMenuItems();
                            },
                            function () {
                                //Do error handling here
                                refLoginData.orgId = '';
                                refLoginData.locId = '';
                                refLoginData.storeId = '';
                                refLoginData.shopId = '';
                                refLoginData.shopName = '';
                                refLoginData.address1 = '';
                                refLoginData.phone = '';
                                refLoginData.fax = '';
                                refLoginData.emailAddr = '';
                                console.log('login failed', refLoginData);
                            });
                    }
                }
            };
        }])
;