angular.module('starter')

    .controller('AppCtrl', ['$scope', '$ionicModal', 'AppData',
        function ($scope, $ionicModal, AppData) {
            // Form data for the login modal
            var self = this;
            self.loginData = AppData.loginData;

            // Create the login modal that we will use later
            $ionicModal.fromTemplateUrl('templates/login.html', {
                scope: $scope
            }).then(function (modal) {
                $scope.modal = modal;
            });

            // Triggered in the login modal to close it
            self.closeLogin = function () {
                $scope.modal.hide();
            };

            // Open the login modal
            self.login = function () {
                $scope.modal.show();
            };

            // Perform the login action when the user submits the login form
            self.doLogin = function () {
                // get shop info
                AppData.login();
                // close
                self.closeLogin();
            };
        }])

    .controller('PlaylistsCtrl', function ($scope) {
        $scope.playlists = [
            {title: 'Reggae', id: 1},
            {title: 'Chill', id: 2},
            {title: 'Dubstep', id: 3},
            {title: 'Indie', id: 4},
            {title: 'Rap', id: 5},
            {title: 'Cowbell', id: 6}
        ];
    })

    .controller('PlaylistCtrl', function ($scope, $stateParams) {
    })

    .controller('HomeMenuController',
    ['AppData', 'CONSTANTS',
        function (AppData, CONSTANTS) {
            // proxy
            var self = this;
            // fields
            self.imageApi = AppData.loginData.apiEntry + CONSTANTS.IMAGE_PATH;
            self.homeMenuItems = AppData.homeMenuItems;
        }])

    .controller('HomeMenuItemController',
    ['$stateParams', 'PosTmpItemFactory', 'CONSTANTS', 'AppData',
        '$ionicPopup',
        function ($stateParams, PosTmpItemFactory, CONSTANTS, AppData,
                  $ionicPopup) {
            // log
            console.log('$stateParams.tmpId', $stateParams.tmpId);
            console.log('$stateParams.name', $stateParams.name);
            // proxy
            var self = this;
            // fields
            self.title = $stateParams.name;
            self.orderItems = AppData.orderItems;
            self.imageApi = AppData.loginData.apiEntry + CONSTANTS.IMAGE_PATH;
            self.drilledMenuItems = PosTmpItemFactory.newResource(AppData.loginData.apiEntry).query({tmpId: $stateParams.tmpId});
            // function
            self.addToOrder = function (i) {
                if (self.orderItems.indexOf(self.drilledMenuItems[i]) === -1) {
                    // push
                    self.orderItems.push(self.drilledMenuItems[i]);
                }

                // always prompt
                $ionicPopup.alert({
                    title: 'Added to Order',
                    template: self.drilledMenuItems[i].name
                });
            };
        }])

    .controller('BrowseController', ['AppData',
        function (AppData) {
            // proxy
            var self = this;
            // fields
            self.loginData = AppData.loginData;
        }])

    .controller('OrderController', [
        'AppData', 'CONSTANTS', '$scope', '$ionicModal', 'PosShopZoneFactory',
        '$ionicSideMenuDelegate', 'PosTmpItemFactory', '$ionicPopup',
        function (AppData, CONSTANTS, $scope, $ionicModal, PosShopZoneFactory,
                  $ionicSideMenuDelegate, PosTmpItemFactory, $ionicPopup) {
            // proxy
            var self = this;
            // fields
            self.orderItems = AppData.orderItems;
            self.imageApi = AppData.loginData.apiEntry + CONSTANTS.IMAGE_PATH;
            self.availableZones = [];
            self.selectedZone = {zoneId: ''};

            // Create the zone modal that we will use later
            $ionicModal.fromTemplateUrl('templates/zone.html', {
                scope: $scope
            }).then(function (modal) {
                $scope.modal = modal;
            });

            // functions

            // Triggered in the zone modal to close it
            self.closeZone = function () {
                $scope.modal.hide();
            };

            // Open the zone modal
            self.showZone = function () {
                // clear
                self.availableZones.length = 0;
                // reload zones
                var refSelf = self;
                PosShopZoneFactory.newResource(AppData.loginData.apiEntry)
                    .query(
                    {shopId: AppData.loginData.shopId},
                    function (results) {
                        //On successful callback run code
                        console.log('reload zone success');
                        // push
                        angular.forEach(results, function (result) {
                            refSelf.availableZones.push(result);
                        });
                        // show
                        $scope.modal.show();
                    },
                    function () {
                        //Do error handling here
                        console.log('reload zone failed');
                    });
            };

            // Perform the order action when the user submits the zone form
            self.doOrder = function () {
                console.log('zoneId: ', self.selectedZone.zoneId);
                // build item key list
                var itemKeyList = "";
                for (var i = 0; i < self.orderItems.length; i++) {
                    if (i > 0) {
                        itemKeyList += ",";
                    }
                    itemKeyList += self.orderItems[i].recKey;
                }
                console.log('itemKeyList: ', itemKeyList);
                // success
                PosTmpItemFactory.newResource(AppData.loginData.apiEntry)
                    .save(
                    {},
                    {
                        charset: "eng",
                        posNo: AppData.loginData.posNo,
                        vipId: "",
                        zoneId: self.selectedZone.zoneId,
                        itemKeyList: itemKeyList
                    },
                    function (results) {
                        $ionicPopup.alert({
                            title: 'Order Submitted',
                            template: 'Server Response:' + results.code
                        });

                        //On successful callback run code
                        console.log('order success', results);
                        // close right
                        $ionicSideMenuDelegate.toggleRight(false);
                        // reset order items
                        self.orderItems.length = 0;
                        // close
                        self.closeZone();
                    },
                    function() {
                        //Do error handling here
                        console.log('order failed');
                    });
            }


            self.clearOrder = function() {
                var confirmClear = $ionicPopup.confirm({
                    title: 'Clear Order',
                    template: 'Are you sure to clear this order?'
                });
                confirmClear.then(function(res) {
                    if(res) {
                        // reset order items
                        self.orderItems.length = 0;
                    } else {
                        //
                    }
                });
            };
        }])
;
